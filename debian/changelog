libmath++ (0.0.4-5) unstable; urgency=medium

  * New maintainer Debian Games Team. (Closes: #634497)
  * Add myself to Uploaders.
  * Do not install NEWS file. It contains nothing noteworthy.
  * Add libmath++-doc.install to install the doxygen documentation.
  * Add watch file and a comment why there is nothing to watch.
  * debian/control:
    - Add rdfind and symlinks to Build-Depends-Indep. Remove duplicate files in
      doc package.
    - Bump Standards-Version to 3.9.5, no changes.
    - Add VCS-fields.
    - Add homepage field.
    - Drop automake, cdbs and libtool. Use dh-autoreconf instead.
    - Use Pre-Depends: ${misc:Pre-Depends}.
    - Drop Conflicts and Replaces fields. They are obsolete.
  * Switch to dh-sequencer.
  * Enable all hardening build flags.
  * Update debian/copyright to copyright format 1.0. The license of libmath++
    is LGPL-2+.
  * debian/patches:
    - Add DEP-3 header to enable_dot.patch.
    - Rename patch filename extension from diff to patch.
  * Add libmath++0c2a.symbols file.

 -- Markus Koschany <apo@gambaru.de>  Tue, 17 Dec 2013 17:26:21 +0100

libmath++ (0.0.4-4) unstable; urgency=low

  * Orphaning package. (See #634497).
  * Build against latest automake, now that libtool is incompatible with
    automake1.7. (Closes: #628307).
  * Remove .la file from -dev package.
  * Multiarchify.
  * Bump Standards-Version to 3.9.2.
  * Update to 3.0 (quilt) source format.

 -- Daniel Schepler <schepler@debian.org>  Mon, 18 Jul 2011 21:12:06 -0700

libmath++ (0.0.4-3) unstable; urgency=low

  * Recompile with new g++ due to allocator changes;
    rename libmath++0c2 -> libmath++0c2a. (Closes: #339210).
  * Add graphviz and gsfonts-x11 to Build-Depends-Indep to get
    improved doxygen docs.

 -- Daniel Schepler <schepler@debian.org>  Mon, 21 Nov 2005 11:19:53 +0100

libmath++ (0.0.4-2) unstable; urgency=low

  * g++-4.0 transition: libmath++0 -> libmath++0c2.
  * Update Standards-Version to 3.6.2 (no changes needed).

 -- Daniel Schepler <schepler@debian.org>  Fri,  8 Jul 2005 15:35:18 -0700

libmath++ (0.0.4-1) unstable; urgency=low

  * New upstream release.
    + Compiles fine with g++-3.4. (Closes: #260597).
  * Update to using automake1.7.

 -- Daniel Schepler <schepler@debian.org>  Thu, 30 Sep 2004 21:30:09 -0700

libmath++ (0.0.3-3) unstable; urgency=low

  * Use binary-install/libmath++-doc instead of install/libmath++-doc;
    the former isn't run by binary-arch, but for some reason the latter
    is. (Closes: #203997).
  * Update section of libmath++-dev to libdevel, to match overrides.

 -- Daniel Schepler <schepler@debian.org>  Mon,  4 Aug 2003 01:30:12 -0700

libmath++ (0.0.3-2) unstable; urgency=low

  * Update to Standards-Version 3.6.0.
    + Use libtool 1.5 to get library with proper binary dependencies.
  * Switch to cdbs.
  * Add examples to libmath++-doc.

 -- Daniel Schepler <schepler@debian.org>  Sun,  3 Aug 2003 02:49:35 -0700

libmath++ (0.0.3-1) unstable; urgency=low

  * Initial Release.

 -- Daniel Schepler <schepler@debian.org>  Sat, 22 Feb 2003 22:30:03 -0800
